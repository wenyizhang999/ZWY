#include<graphics.h>
#include<conio.h>
#pragma comment(lib,"Winmm.lib")
IMAGE img_bk, img_bd1, img_bd2, img_bar_up1, img_bar_up2, img_bar_down1, img_bar_down2;
int bird_x;
int bird_y; 

void startup() 
{
	initgraph(350, 600);
	loadimage(&img_bk, "C:\\Users\\VULCAN\\Pictures\\作业\\bkbrid.png");
	loadimage(&img_bd1, "C:\\Users\\VULCAN\\Pictures\\作业\\brid1.png");
	loadimage(&img_bd2, "C:\\Users\\VULCAN\\Pictures\\作业\\brid2.png");
	loadimage(&img_bar_up1, "C:\\Users\\VULCAN\\Pictures\\作业\\up1.png");
	loadimage(&img_bar_up2, "C:\\Users\\VULCAN\\Pictures\\作业\\up2.png");
	loadimage(&img_bar_down1, "C:\\Users\\VULCAN\\Pictures\\作业\\down1.png");
	loadimage(&img_bar_down2, "C:\\Users\\VULCAN\\Pictures\\作业\\down2.png");
	bird_x = 50;
	bird_y = 200;
	BeginBatchDraw();

	mciSendString("open C:\\Users\\VULCAN\\Desktop\\11303.mp3", NULL, 0, NULL);
	mciSendString("play bkmusic repeat", NULL, 0, NULL);
}

void show() 
{
	putimage(0, 0, &img_bk);
	putimage(150, -300, &img_bar_up1, NOTSRCERASE);
	putimage(150, -300, &img_bar_up2, SRCINVERT);
	putimage(150, 400, &img_bar_down1, NOTSRCERASE);
	putimage(150, 400, &img_bar_down2, SRCINVERT);
	putimage(bird_x, bird_y, &img_bd1, NOTSRCERASE);
	putimage(bird_x, bird_y, &img_bd2, SRCINVERT);
	FlushBatchDraw();
	Sleep(50);
}

void updateWithoutInput() 
{
	if (bird_y < 500)
		bird_y = bird_y + 3;
}

void updateWithInput()
{
	char input;
    if (kbhit())
    {
		input = getch();
		if (input ==  ' ' && bird_y > 20)
				{
					bird_y = bird_y - 60;
					mciSendString("close 11303.mp3", NULL, 0, NULL);
					mciSendString("open C:\\Users\\VULCAN\\Desktop\\11303.mp3 ", NULL, 0, NULL);
					mciSendString("play 11303.mp3", NULL, 0, NULL);
				}
		}
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main()
{
	startup(); //数据初始化 
	while (1)
	{
		show(); //显示界面 
		updateWithoutInput(); //与用户输入无关的更新
		updateWithInput(); //与用户输入有关的更新 
	}
	gameover();
	return 0;
}
