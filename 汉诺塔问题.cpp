#include<stdio.h>
void hanio (int n,char a,char b,char c)  // n表示盘子总数，   设置参数，说明通过b这个辅助柱子把A中的盘子移动至c
{
    if(n==1)
    {
    printf("%d: %c -> %c\n",n,a,b);  //如果只有一个盘子就不需要方法，直接移动
    }
    else{
        hanio (n-1,a,c,b);   //如果不止一个盘子则调用n-1个盘子的方法，把上面的n-1个盘子由a，借助c移动到b，a就只剩一个最大的了
        printf("%d: %c -> %c\n",n,a,b);  //把a里剩下的最大的盘子移动至b
        hanio(n-1,c,b,a); //c里最大的，b有n-1个排好的盘子，同样用移动n-1个盘子的方法把n-1个盘子移动至c
        }
}
int main()
{
    int n;
    char x,y,z;
    scanf("%d\n",&n);
    scanf("%c %c %c",&x,&y,&z);
    hanio(n,x,y,z);  //输出答案
} 
