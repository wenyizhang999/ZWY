#include <stdio.h>
#include <math.h>

int prime( int p ){
	int i;
	int r;
	if(p<2){
		return 0;
	}
	for(i=2;i<=(int)sqrt(p);i++){//这里其实也可以i*i<p应该是一样的，目的都是减少循环次数
		r=p%i;
		if(r==0){//判断是否为素数，能整除那就凉凉肯定不是素数，返回值为0
			return 0;
		}
	}
	return 1;//如果上面都熬下来了，说明恭喜你成为了素数
}
void Goldbach( int n ){
	int i=3;//因为所求解的左边值是小的，所以由左边从小增大
while(i<=n/2){//因为对称性所以只需要看一半就可以了，剩下一半是重复的！
		if(prime(i)==1&&prime(n-i)==1){//判断两个值是否为素数
			printf("%d=%d+%d",n,i,n-i);
			break;
		}
		i+=2; 
	}

}

